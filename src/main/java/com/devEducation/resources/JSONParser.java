package com.devEducation.resources;

import com.devEducation.model.Song;
import com.google.gson.Gson;

import java.util.List;

public class JSONParser {
    public static String parseListSongsToJson(List<Song> songList) {
        Gson gson = new Gson();
        return gson.toJson(songList);
    }
}

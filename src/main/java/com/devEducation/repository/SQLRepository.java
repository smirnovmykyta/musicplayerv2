package com.devEducation.repository;

import com.devEducation.model.Song;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class SQLRepository {
    private static final String URL = "jdbc:mysql://localhost:3306/player?serverTimezone=Europe/Moscow&useSSL=false";
    private static final String USER = "root";
    private static final String PASSWORD = "root";
    private Statement statement;

    private static final Logger logger = LoggerFactory.getLogger(SQLRepository.class);

    private Connection connection;
    private ResultSet result;

    public void connectionToDB() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection(URL, USER, PASSWORD);
            if (connection != null) {
                logger.debug("Connection to ProductDB succesfull!");
            }
        } catch (Exception ex) {
            logger.error("Connection failed...\n" + ex);
        }
        createTables();
    }

    private void createTables() {
        try {
            statement = connection.createStatement();
            String createTable = new StringBuilder("CREATE TABLE IF NOT EXISTS song(")
                    .append("title   VARCHAR (100) NOT NULL,")
                    .append("artist VARCHAR (100) NOT NULL,")
                    .append("genre VARCHAR(100) NOT NULL,")
                    .append("path VARCHAR (100) NOT NULL);").toString();
            statement.executeUpdate(createTable);

            logger.debug("Tables successfully created");
        } catch (SQLException e) {
            logger.error("Tables wasn't created");
            e.printStackTrace();
        }
    }

    public void clearTable() {
        try {
            statement = connection.createStatement();
            String query = "TRUNCATE TABLE song";
            statement.executeUpdate(query);

            logger.debug("Tables successfully clear");
        } catch (SQLException e) {
            logger.error("Tables wasn't clear");
            e.printStackTrace();
        }
    }

    public void addSongsToTable(List<Song> listOfSongs) throws SQLException {
        for (Song song : listOfSongs) {
            String query = new StringBuilder("INSERT INTO player.song (title, artist, genre, path)")
                    .append("VALUES ('")
                    .append(song.getName()).append("','")
                    .append(song.getArtist()).append("','")
                    .append(song.getGenre()).append("','")
                    .append(song.getPath())
                    .append("');").toString();
            statement.executeUpdate(query);
        }
    }

    public String getGenres() throws SQLException {
        String query = "SELECT distinct genre FROM song;";
        String genres = "";

        statement = connection.createStatement();
        result = statement.executeQuery(query);
        while (result.next()) {
            genres = new StringBuilder(result.getString(1)).append(",").toString();
        }
        return genres;
    }

    public String getArtists(String genre) throws SQLException {
        String query = "SELECT distinct artist FROM song where genre='" + genre + "';";
        String artists = "";

        statement = connection.createStatement();
        result = statement.executeQuery(query);
        while (result.next()) {
            artists = new StringBuilder(result.getString(1)).append(",").toString();
        }
        return artists;
    }

    public List<Song> getListOfSongsByGenreAndArtist(String genre, String artist) throws SQLException {
        String query = new StringBuilder("SELECT distinct title, artist, path FROM song WHERE genre='")
                .append(genre)
                .append("' AND artist='")
                .append(artist)
                .append("';").toString();
        List<Song> listOfSongs = new ArrayList<Song>();

        statement = connection.createStatement();
        result = statement.executeQuery(query);
        while (result.next()) {
            String title = result.getString(1);
            String artists = result.getString(2);
            String path = result.getString(3);
            listOfSongs.add(new Song(path, title, artists, genre));
        }
        return listOfSongs;
    }
}

package com.devEducation.servlet;

import com.devEducation.model.Song;
import com.devEducation.repository.SQLRepository;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;
import org.apache.tika.parser.mp3.Mp3Parser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(urlPatterns = "/addPathToDB")
public class MusicServlet extends HttpServlet {
    private final static long serialVersionUID = 1L;

    private static final Logger logger = LoggerFactory.getLogger(MusicServlet.class);

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        PrintWriter printWriter = response.getWriter();
        response.setContentType("application/json");

        String path = request.getParameter("path");
        SQLRepository sqlRepository = new SQLRepository();
        sqlRepository.clearTable();
        try {
            sqlRepository.addSongsToTable(getDataAboutSong(path));
            printWriter.print(sqlRepository.getGenres());
        } catch (SQLException e) {
            logger.error("Error get genres from SQL DB ");
            e.printStackTrace();
        }
        printWriter.flush();
    }

    private List<Song> getDataAboutSong(String pathName) {
        File folder = new File(pathName);
        File[] listOfFiles = folder.listFiles((dir, name) -> name.toLowerCase().endsWith(".mp3"));
        ArrayList<Song> songs = new ArrayList<>();

        for (File file : listOfFiles) {
            if (file.isFile()) {
                try {
                    String inputFile = pathName + "//" + file.getName();
                    String absolutePathSong = file.getAbsolutePath().replace("\\", "/");
                    InputStream input = new FileInputStream(inputFile);
                    ContentHandler handler = new DefaultHandler();
                    Metadata metadata = new Metadata();
                    Parser parser = new Mp3Parser();
                    ParseContext parseCtx = new ParseContext();
                    parser.parse(input, handler, metadata, parseCtx);
                    input.close();

                    songs.add(new Song(absolutePathSong, metadata.get("title"), metadata.get("xmpDM:artist"), metadata.get("xmpDM:genre")));
                } catch (FileNotFoundException e) {
                    logger.error("File not found ");
                    e.printStackTrace();
                } catch (IOException e) {
                    logger.error("IO exception ");
                    e.printStackTrace();
                } catch (SAXException e) {
                    logger.error("SAX exception ");
                    e.printStackTrace();
                } catch (TikaException e) {
                    logger.error("Get tags exception ");
                    e.printStackTrace();
                }
            }
        }
        return songs;
    }

}

package com.devEducation.servlet;

import com.devEducation.model.Song;
import com.devEducation.repository.SQLRepository;
import com.devEducation.resources.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;

@WebServlet(urlPatterns = "/getSong")
public class SongGenreServlet extends HttpServlet {
    private static final Logger logger = LoggerFactory.getLogger(SongGenreServlet.class);


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");

        String genre = request.getParameter("genre");
        String artist = request.getParameter("artist");
        SQLRepository sqlRepository = new SQLRepository();
        try {
            List<Song> list = sqlRepository.getListOfSongsByGenreAndArtist(genre, artist);
            out.print(JSONParser.parseListSongsToJson(list));
        } catch (SQLException e) {
            logger.error("Error get genres from SQL DB ");

            e.printStackTrace();
        }
        out.flush();
    }
}

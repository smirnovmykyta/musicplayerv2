package com.devEducation.servlet;

import com.devEducation.repository.SQLRepository;
import com.devEducation.repository.SQLRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

@WebServlet(urlPatterns = "/getArtist")
public class ArtistServlet extends HttpServlet {
    private static final Logger logger = LoggerFactory.getLogger(ArtistServlet.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        PrintWriter out = response.getWriter();
        response.setCharacterEncoding("UTF-8");

        String genre = request.getParameter("genre");
        SQLRepository sqlRepository = new SQLRepository();
        try {
            out.print(sqlRepository.getArtists(genre));
            logger.debug("Genres were received");
        } catch (SQLException e) {
            logger.error("Genres weren't received");
            e.printStackTrace();
        }
        out.flush();
    }
}


package com.devEducation.model;

public class Song {
    private String name;
    private String artist;
    private String genre;
    private String path;

    public Song(String name, String artist, String genre, String path) {
        this.name = name;
        this.artist = artist;
        this.genre = genre;
        this.path = path;
    }

    public String getName() {
        return name;
    }

    public String getArtist() {
        return artist;
    }

    public String getGenre() {
        return genre;
    }

    public String getPath() {
        return path;
    }

    @Override
    public String toString() {
        return "Song{" +
                "name='" + name + '\'' +
                ", artist='" + artist + '\'' +
                ", genre='" + genre + '\'' +
                ", path='" + path + '\'' +
                '}';
    }
}
